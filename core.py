from bs4 import BeautifulSoup
import requests
url ="https://www.codewithharry.com"

# response = requests.get(url).content  -------- .content gives the html content of the given url
req = requests.get(url)

soup = BeautifulSoup(req.text, "html.parser")
# print(soup.title)
# # returns the title of the html content
for link in soup.find_all('a'):
    print(link.get('href')) # if without the .get'href we get the html content + link 
